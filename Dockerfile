FROM python:2.7.18-alpine3.11
WORKDIR /usr/src/app
RUN apk add --no-cache --virtual .build-deps gcc libc-dev && pip install --no-cache-dir snmpsim && apk del .build-deps
COPY snmpsim.sh ./
COPY hp ./
CMD [ "./snmpsim.sh" ]
